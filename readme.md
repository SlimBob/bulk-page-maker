# Bulk Page Maker #
* Contributors: robertwhitis
* Donate link: http://slimbobwp.com/
* Tags: page, pages, edit, editor, bulk, post, posts, lorem ipsum, import, multisite, network, duplicate, copy,
* Requires at least: 4.1.1
* Tested up to: 4.4
* Stable tag: 4.4
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html

### Bulk publish pages or posts! Options include title, slug, parent, page template, status, post format, and content via the default WordPress Editor. ###

## Description ##

Bulk publish pages or posts! Options include title, slug, parent, page template, status, post format, and content via the default WordPress Editor.

### Current Features: ###

* Allows Bulk Creation of Posts or Pages
* Supports Posts and Pages

### Current Creation Variables: ###

* Default Content (Uses Default WordPress Editor UI)
* Title
* Slug (Optional)
* Parent
* Page Template
* Post Format
* Status

## Installation ##

1. Upload the `/bulk-page-maker/` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.

## Frequently Asked Questions ##

* None at this time.

## Screenshots ##

** Pages > Bulk Add New **

![screenshot-1.png](https://bitbucket.org/repo/xq6pg6/images/428561810-screenshot-1.png)


** Posts > Bulk Add New **

![screenshot-2.png](https://bitbucket.org/repo/xq6pg6/images/3013429339-screenshot-2.png)


## Changelog ##

### 1.0.0 ###
* Initial release.

### 1.0.1 ###
* Fixed missing admin menu link on non-multisite installations.

### 1.0.2 ###
* Adds "(Optional)" text to field label for Slug column.
* Adds default WordPress styling for success message.

### 1.0.3 ###
* Code cleanup.

### 1.1 ###
* Adds support for native post type, including post formats.

### 1.1.1 ###
* Updates description to include posts as well as pages.

### 1.1.2 ###
* Updates tags to include posts and bumps stable tag to 4.4.

### 1.1.3 ###
* Updates FAQs and descriptions.

### 2.0.0 ###
* Ability to add or remove field rows as needed, instead of using 20 static field rows.
* Introduce logic to only display Post Format option if the current theme supports Post Formats.
* Introduce logic to only display Page Template option if the current theme has Page Templates.
* Change display of Parent page select field to be hierarchical.
* Addition of Settings page to set default options for Page Content, Template, Post Format, and Status.
* Support for custom post types.